
# **Toma de decisiones**

![Toma de decisiones](img/Toma_de_decisiones.png "Toma de decisiones")

Regla nemotecnica "quecocudo", Que Como Cuando y Donde. En la toma de decisiones estas preguntas modifican en la forma en la que tomamos decisiones.

**Tipos de decisiciones**

* **Certidumbre** : Es conocer o tener toda la informacion necesaria para tomar la mejor decision.
* **Riesgo** : Tenemos algo de informacion que acompaña a esa decision.
* **Incertidumbre** : No se tiene informacion y tampoco hay forma de calcular el riesgo.

**Video** : [Toma de decisiones en el reino animal](https://youtu.be/Y1RUPrCbwgg "Toma de decisiones en el reino animal")

---
## **Proceso para la toma de decision eficaz**

![Proceso para la toma de decision eficaz](img/Proceso_para_la_toma_de_decision_eficaz.png "Proceso para la toma de decision eficaz")

---
## **Etapas para la eleccion de alternativas**

1. Identificar el problema
2. Identificar criterios para tomar decisiones
3. Asignar pesos a los criterios
4. Desarrollar las alternativas
5. Analizar las alternativas
6. Seleccionar las alternativa necesaria
7. Implementar la alternativa elegida
8. Analizar el resultado de la solucion

**Video** : [Etapas para la eleccion de alternativas](https://youtu.be/cFr7e3RsjPY "Etapas para la eleccion de alternativas")

---
## **Tipos de tomas de decisiones**

![Tipos de tomas de decisiones](img/Tipos_de_tomas_de_decisiones.png "Tipos de tomas de decisiones")

---
## **Decisiones reales creativas**

**Dilema moral**

Estas conduciendo tu auto de noche y con una tremenda tormenta. Pasa por una parada de colectivo donde hay 3 personas esperando:

1. Una persona de muchos años y con aspecto desmejorado.
2. Una persona amiga a la que quieres mucho.
3. La persona de tus sueños.

¿ A cual llevarias en el coche, sabiendo que solo tienes sitio para una sola persona ?

Este es el dilema etico-moral que una vez se utilizo en una entrevista de trabajo. La persona que fue contratada (de entre 200) no dudo al dar su respuesta.

**"Le daria las llaves del auto a esa persona amiga, y le diria que llevara a la persona de muchos años al hospital, mientras yo me quedaria esperando el colectivo con la persona de mis sueños"**







