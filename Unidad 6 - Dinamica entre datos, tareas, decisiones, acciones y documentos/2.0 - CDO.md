
# **¿ Que son los CDO ? - Chief data Officer - Oficinas centrales de datos**

**Transformaciones digital y cultural**

Las diversas organizaciones necesitan tomar sus decisiones basadas en evidencia. Con una estrategia integral de datos, se promueve la innovacion constante, la toma de desiciones de datos y produce mejoras en los productos o serrvicios que se brindan a los interesados.

* **Situacion actual (Gobernanza de datos descentralizada)**
    - **Ausencia de autoridad** en materia de datos.
    - **Dificultad de coordinacion** de estrategia.
    - Altos **costos de transaccion**.
    - **Capacidad analiticas dispares** entre areas.
    - **Impacto limitado** de proyectos.

* **Situacion con CDO (Gobierno de datos)**
    - **Nueva institucionalidad** de datos.
    - **Estrategia** de datos unificada.
    - **Intercambio** de datos fluido entre areas.
    - Mayor **capacidad analitica** en areas.
    - **Agilidad** en implementacion de proyectos.

---
## **Funciones primarias del CDO**

**Es un area de servicios**

* Desarrollar y fortalecer una cultura de datos dentro de la organizacion.
* Articular con los lideres de datos en las diversas areas.
* Implementar el inventario de Metadatos de la empresa (son los metadatos, no los datos que los mantiene el area).
* Establecer la estrategia integral de datos dentro, esto es, de que modo y bajo que estandares se generan e intercambian los datos para su interoperabilidad.
* Desarrollar los criterios para clasificar los grados de sensibilidad y niveles de acceso a los datos (aquel que es abierto/interno/restringido/secreto).
* Realizar la explotacion de datos de proyectos estrategicos para toma de desiciones (tableros de control, modelos predictivos, mapas de gestion).
* Realizar la ingenieria y definir la arquitectura de los datos para facilitar su explotacion.
* Resguardar legalmente el correcto acceso y uso de datos, estableciendo protocolos ante filtraciones.
* Revisar contrataciones donde se incluyan aspectos relativos a la gestion y explotacion de datos.

---
## **Objetivos del CDO**

![Objetivos del CDO](img/Objetivos_del_CDO.png "Objetivos del CDO")

---
## **Capacidades centralizadas vs Descentralizadas**

![Capacidades centralizadas vs Descentralizadas](img/Capacidades_centralizadas_vs_Descentralizadas.png "Capacidades centralizadas vs Descentralizadas")

---
## **Oportunidad de mejora de un proceso de informacion**

![Oportunidad de mejora de un proceso de informacion](img/Oportunidad_de_mejora_de_un_proceso_de_informacion.png "Oportunidad de mejora de un proceso de informacion")

---
## **Desarrollo de un nuevo sistema**

![Desarrollo de un nuevo sistema](img/Desarrollo_de_un_nuevo_sistema.png "Desarrollo de un nuevo sistema")

---
## **Proyecto de explotacion prioritario**

![Proyecto de explotacion prioritario](img/Proyecto_de_explotacion_prioritario.png "Proyecto de explotacion prioritario")

---
## **Proyecto de explotacion no prioritario**

![Proyecto de explotacion no prioritario](img/Proyecto_de_explotacion_no_prioritario.png "Proyecto de explotacion no prioritario")

---
## **Clasificacion de sensibilidad**

![Clasificacion de sensibilidad](img/Clasificacion_de_sensibilidad.png "Clasificacion de sensibilidad")

---
## **Intercambio de datos**

![Intercambio de datos](img/Intercambio_de_datos.png "Intercambio de datos")

---
## **Definicion de un nuevo estandar**

![Definicion de un nuevo estandar](img/Defninicion_de_un_nuevo_estandar.png "Definicion de un nuevo estandar")

---
## **Adquisicion de bases de datos**

![Adquisicion de bases de datos](img/Adquisicion_de_bases_de_datos.png "Adquisicion de bases de datos")
