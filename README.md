
# **Curso IALAB - Gobernanza de Datos**

Curso de Inteligencia Artificial - Gobernanza de Datos.

## **Unidad 1: Introducción a la IA**

* Contenido de la Unidad 1 

1. Introducción a la Inteligencia Artificial
2. ¿Qué es la IA?
3. ABC de la inteligencia humana. Punto A: Reconocimiento de patrones de información
4. ABC de la inteligencia humana. Punto B: Adaptación del cerebro humano
5. ABC de la inteligencia humana. Punto C: Límites del procesamiento de la información en personas humanas
6. ABC del concepto de Inteligencia Artificial. Punto A: Fenómeno del blanco móvil
7. Introducción a los datos, información, conocimiento y algoritmos
8. ABC del concepto de Inteligencia Artificial. Punto C: Capacidad de reconocer patrones de información
9. La criatura nueva, definiciones de IA, interpretaciones
10. Motivaciones de la IA
11. Aplicaciones de la IA
12. Oportunidades de la IA
13. Desafíos de la IA
14. IA. ¿Oportunidad o desafío?
15. Acciones. Uso de la IA para los servicios públicos
16. Conclusiones acerca de las aplicaciones, oportunidades, desafíos y acciones de la IA
17. Estado del arte. Rol de la IA en los Estados
18. Gobernanza y estado de la Inteligencia Artificial en los Estados de la región
19. Impacto de la IA en temáticas de radical importancia para la región
20. Conclusiones sobre oportunidades y desafíos de la IA para la región
21. El impacto de la IA en el trabajo. Introducción
22. ¿Las máquinas nos desemplean?
23. Los trabajos y tareas que surgen en la era de la IA
24. Acelerar la reconversión y proteger a las personas más vulnerables
25. Paradojas tecnológicas de la historia: cómo aprovecharlas para enfocar la inteligencia artificial de manera sostenible
26. Ruta para poner en marcha la IA
27. Primera tutoría sincrónica con presencialidad virtual (Unidad 1)

* Test de Lectura

## **Unidad 2: Introducción a la gobernanza de datos**

* Contenido de la Unidad 2

1. ¿Qué son los datos?
2. ¿Cómo hacer un buen gobierno de datos?
3. Estructuras organizacionales
4. Black Mirror "Bandersnatch" y organizaciones
5. Gobernanza de la infraestructura de datos
6. Trabajos vs. Tareas
7. Actividades básicas previas para la gobernanza de datos
8. Consideraciones éticas de a gobernanza de datos

* Test de Lectura

## **Unidad 3: Representación de procesos decisionales y gestión de la documentación**

* Contenido de la Unidad 3

1. Toma de decisiones
2. Proceso para la toma de decisión eficaz
3. Etapas para la elección de alternativas
4. Tipos de tomas de decisiones
5. Decisiones reales creativas
6. Implementaciones reales - Decisiones (parte 2)
7. Implementaciones reales - Decisiones (parte 3)
8. Implementaciones reales - Decisiones (parte 4)
9. Implementaciones reales - Decisiones (parte 5)
10. Implementaciones reales - Decisiones (parte 6)

* Test de Lectura

## **Unidad 4: Elaboración de requerimientos de los sistemas de IA**

* Contenido de la Unidad 4

1. Gobernanza de datos (Introducción)
2. ¿Y eso qué sería?
3. ¿Qué problemas podemos encontrar con la gobernanza?
4. Estándares y buenas prácticas (1)
5. Estándares y buenas prácticas (2)
6. Catálogo de datos
7. Roles y responsabilidades en el uso de los datos
8. Autogestión y uso eficiente de los datos
9. Ejemplos de servicios de Telecomunicaciones (parte 1)
10. Estratégicos: Políticos y principios (1)
11. Estratégicos: Políticos y principios (2)
12. Ejemplos de servicios de Telecomunicaciones (parte 2)
13. Estratégicos: Comité de datos
14. Estratégicos: Procesos
15. Estándares: Arquitectura
16. Estándares: Calidad
17. Estándares: Privacidad y cumplimiento
18. Estándares: Metadata
19. Estándares: Seguridad, accesos y usos
20. Estándares: Compra y monetización
21. Herramientas de Gobierno

* Test de Lectura

## **Unidad 5: Lógicas de diálogo e interacciones intuitivas entre usuarios/as**

* Contenido de la Unidad 5

1. Datos o modelos inseguros
2. Mal modelo o definición de negocios
3. Mala calidad de datos
4. Chatbots (parte 1)
5. ¿Qué es un chatbot?
6. ¿Qué es un asistente digital?
7. Chatbots (parte 2)
8. Chatbots (parte 3)
9. Chatbots: Modelos generativos (Tay)
10. Chatbots: Diseño de diálogos representado con diagrama de flujos
11. Obtener información sobre el diseño de un bot conversacional
12. Diseñador de conversaciones
13. Gobernanza de datos, agentes conversacionales y matices de automatización
14. Segunda tutoría sincrónica con presencialidad virtual (Unidad 2 a Unidad 5)

* Test de Lectura

## **Unidad 6: Dinámica entre datos, tareas, decisiones, acciones y documentos - Etiquetado de datos**

* Contenido de la Unidad 6

1. CDO
2. Funciones primarias del CDO
3. Objetivos del CDO
4. CDO: Dinámica
5. Oportunidad de mejora de un proceso de información
6. Desarrollo de un nuevo sistema
7. Proyecto de explotación prioritario
8. Proyecto de explotación no prioritario
9. Clasificación de sensibilidad
10. Intercambio de datos
11. Definición de un nuevo estándar
12. Adquisición de bases de datos

* Test de Lectura

## **Unidad 7: Muestra de datos, diseño de datasets y reducción de sesgos**

* Contenido de la Unidad 7

1. Censos vs. muestras
2. Censos conocidos
3. Muestras
4. Muestras y Big Data
5. Muestras - training, validation y test
6. Sesgos
7. ¿Cómo se produce el sesgo algorítmico y por qué es tan difícil detenerlo?
8. Ejemplos prácticos (1)
9. Ejemplos prácticos (2)
10. Ejemplos prácticos (3)
11. Ejemplos prácticos (4)
12. Gobernanza de datos para predicciones trazables

* Test de Lectura

## **Unidad 8: Definición e identificación de criterios - Segmentación de datasets - Curado y etiquetado de datos**

* Contenido de la Unidad 8

1. Preprocesamiento de datos
2. Comprobaciones básicas
3. Tareas principales
4. Tratamientos de datos faltantes
5. Normalizar datos
6. Discretizar datos
7. Reducir datos
8. Limpiar datos de texto
9. Ejemplos prácticos (1)
10. Ejemplos prácticos (2)
11. Tareas para preparar los datos para el aprendizaje automático mejorado
12. Tercera tutoría sincrónica con presencialidad virtual (Unidad 6 a Unidad 8)

* Test de Lectura

## **Desafío**

1. Ejercicio práctico: Gobernanza de datos para detección en materia de Tránsito (problemas vinculados al procesamiento de lenguaje natural)
2. Cuarta tutoría sincrónica con presencialidad virtual (Ejercicio práctico)
